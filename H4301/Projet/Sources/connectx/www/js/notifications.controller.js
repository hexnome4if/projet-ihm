app.controller('notificationsController', function($rootScope, $scope, $mdToast, $location) {

    $scope.customFullscreen = false;

    $scope.tryBack = function() {

        if ($rootScope.getNotifications().length == 0) {
            $rootScope.back();
        }

    };

	$scope.openDetails = function (idDemande) {

		$location.url("/detail-echange?idDemande=" + idDemande)

	};

    $scope.showRefuse = function(ev, idDemande) {
        $scope.status = 'Demande refusée.';
        $rootScope.data.demandes[idDemande].statut = 'refusée';

        var dateNow = new Date();
        var h = dateNow.getHours();
        var m = dateNow.getMinutes();
        var heure = h + 'h' + m;

        var y = dateNow.getYear();
        var mo = dateNow.getMonth();
        var d = dateNow.getDate();
        var date = d + '/' + mo + '/' + '20' + (y - 100);

        $scope.showSimpleToast("Demande refusée");

        $scope.tryBack();
    };

    $scope.showSimpleToast = function(text) {
        $mdToast.show(
            $mdToast.simple()
            .textContent(text)
            .position('bottom center')
            .hideDelay(2000)
        );
    };

});
