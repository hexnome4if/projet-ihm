app.controller('FilterController', function($rootScope, $scope, $location, $routeParams) {

	$scope.query = $routeParams.query;
	$scope.category = $routeParams.category;
	$scope.distance = $routeParams.distance;
	$scope.price = $routeParams.price;

	$scope.category = ($scope.category == "undefined") ? "" : $scope.category;
	$scope.distance = ($scope.distance == "undefined") ? "" : $scope.distance;
	$scope.price = ($scope.price == "undefined") ? "" : $scope.price;

	$scope.validate = function () {

		$location.url("/find-product?query=" + encodeURIComponent($scope.query) + "&category=" + encodeURIComponent($scope.category) + "&distance=" + encodeURIComponent($scope.distance) + "&price=" + encodeURIComponent($scope.price));


	};

});
