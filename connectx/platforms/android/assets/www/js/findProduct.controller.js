app.controller('FindProductController', function($rootScope, $scope, $routeParams, $mdDialog, $mdToast, $location) {

	$scope.query = $routeParams.query || "";
	$scope.category = $routeParams.category;
	$scope.distance = $routeParams.distance;
	$scope.price = $routeParams.price;

	$scope.category = ($scope.category == undefined || $scope.category == "undefined") ? "" : $scope.category;
	$scope.distance = ($scope.distance == undefined || $scope.distance == "undefined") ? "" : $scope.distance;
	$scope.price = ($scope.price == undefined || $scope.price == "undefined") ? "" : $scope.price;

    $scope.getUserById = function(userId) {

        return $rootScope.data.profil.find(function(profil) {
            return profil.id == userId;
        });

    };

    $scope.search = function() {

        var results = [];

        for (var o in $rootScope.data.objets) {

            var objet = $rootScope.data.objets[o];

            if (objet.idProprietaire != 0 && objet.nom.toLowerCase().indexOf($scope.query.toLowerCase()) != -1) {

                results.push(objet);
            }

        }

        return results;

    };

    $scope.randomDistance = [
		"500m", "1km", "200m", "1km", "6km", "900m", "1km", "1km", "500m", "600m", "42m", "1km", "100m", "800m",
		 "2km", "7km","200m", "1km", "6km", "900m", "1km", "50m", "3km", "9km", "700m", "1km", "1km", "500m", "600m",
		 "42m", "900m", "2km", "1km", "9km", "6km", "200m", "150m", "300m"];

	$scope.editFilters = function() {

		$location.url("/filter?query=" + encodeURIComponent($scope.query) + "&category=" + encodeURIComponent($routeParams.category) + "&distance=" + encodeURIComponent($routeParams.distance) + "&price=" + encodeURIComponent($routeParams.price));

	};

    $scope.filters = function() {

		var filters = [];

		if($scope.category != "") {
			filters.push($scope.category);
		}

		if($scope.distance != "") {
			filters.push($scope.distance);
		}

		if($scope.price != "") {
			filters.push($scope.price);
		}


        return filters;
    }

	$scope.filterPriceAbs = function (price) {

		if(price == "Gratuit") {
			return 0;
		}

		return parseInt(String(price).replace(/[^0-9]/g, ""));
	};

	$scope.filterDistanceAbs = function (distance) {

		return parseInt(String(distance).replace("k", "000").replace(/[^0-9]/g, ""))
	};

	$scope.filtered = function (object, distance) {

		if($scope.category != "") {
			if($scope.category != object.type) {
				return true;
			}
		}

		if($scope.distance != "") {
			if($scope.filterDistanceAbs(distance) > $scope.filterDistanceAbs($scope.distance)) {
				return true;
			}
		}

		if($scope.price != "") {
			if($scope.filterPriceAbs(object.prix) > $scope.filterPriceAbs($scope.price)) {
				return true;
			}
		}


		return false;
	}

    $scope.showSimpleToast = function(text) {
        $mdToast.show(
            $mdToast.simple()
            .textContent(text)
            .position('bottom center')
            .hideDelay(3000)
        );
    };

	$scope.requestFor = function (objet, distance) {

		var html = "";

		html += "<p><b>Catégorie</b><br /> " + objet.type + "<p>";
		html += "<p><b>Description</b><br /> " + objet.infos + "<p>";
		html += "<p><b>Prêteur</b><br /> " + $rootScope.data.profil[objet.idProprietaire].nom + "<p>";
		html += "<p><b>Prix</b><br /> " + objet.prix + objet.devise + objet.prixType + "</p>";
		html += "<p><b>Distance</b><br /> " + distance + "</p>";

		var confirm = $mdDialog.confirm()
	          .title(objet.nom)
	          .htmlContent(html)
	          .ariaLabel('')
	          .ok('Demander')
	          .cancel('Annuler');

	    $mdDialog.show(confirm).then(function() {

			var id = $rootScope.data.demandes.length;

			$rootScope.data.demandes.push({
				'id': id,
				'idProprietaire': objet.idProprietaire,
				'idObjet': objet.id,
				'idDemandeur': 0,
				'distance': distance,
				'dateDemande': getNow().date,
				'heureDemande': getNow().time,
				'dateDebut': null,
				'dateFin': null,
				'statut': 'en attente',
				'chat': [{
					"type": "center",
					"content": "start"
				},
				{
					"type": "requester",
					"content": "Bonjour, je souhaiterais vous emprunter votre " + objet.nom + " si cela vous est possible."
				}
			]});

			$scope.showSimpleToast("Votre demande a bien été envoyée");

			$location.url('/');

	    }, function() {
	    });



	};

    $scope.broadcast = function() {

        var prompt = $mdDialog.prompt({
            title: 'Diffuser une demande',
            htmlContent: "Entrer le <b>nom de l'objet</b> que vous rechercher au près des autres utilisateurs.",
            placeholder: "Nom de l'objet",
            initialValue: $scope.query,
            ok: 'Diffuser',
            cancel: 'Annuler'
        });

        $mdDialog
            .show(prompt)
            .then(function(name) {

				if(name == undefined || name.length == 0) {
					$scope.showSimpleToast("Le nom de l'objet ne peut être vide");
					return;
				}


                var d = new Date();

                var date = d.getDate() + "/" + d.getMonth() + "/" + d.getYear();
                var hour = d.getHours() + "h" + d.getMinutes();

                $rootScope.data.demandes.push({
                    'id': $rootScope.data.demandes.length,
                    'idProprietaire': null,
                    'idObjet': null,
                    'nomObjet': name,
                    'idDemandeur': 0,
                    'distance': '500 m',
                    'dateDemande': getNow().date,
                    'heureDemande': getNow().time,
                    'dateDebut': null,
                    'dateFin': null,
                    'statut': 'en attente',
                    'chat': [],
                    'historique': [{
                        'action': 'Demande de prêt reçue',
                        'date': getNow().date,
                        'heure': getNow().time
                    }]
                });

                $scope.showSimpleToast("Votre demande a bien été diffusée");

                $location.url("/");
            })
            .catch(function() {
                //VOID
            });

    };

});
