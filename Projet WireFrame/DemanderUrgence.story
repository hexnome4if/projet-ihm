<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels id="iuinrp5PBCdJ9PqhIatocx9HJ8s=" x="75" y="75">
    <screen href="ChoixInitial/ChoixInital_Alt1.screen#/"/>
  </panels>
  <panels id="mC5QB6FokT0-xbARZNIQQ7Rncbs=" x="375" y="75">
    <screen href="ChoixTypeDemande/ChoixDemande_Alt1.screen#/"/>
  </panels>
  <panels id="OK8v1JCP683fEx8cNV0xbFWRZFc=" x="675" y="75">
    <screen href="ChoixDesBesoins/ChoixBesoin_Alt1.screen#/"/>
  </panels>
  <panels id="G5dc_NtEqnr0ddAVtGy7jyTRpLk=" x="975" y="75">
    <screen href="ChoixDesCables/ChoixCable_Alt1.screen#/"/>
  </panels>
  <panels id="26K1k6NGPYeE_mftxPJDcTPGQBU=" x="75" y="375">
    <screen href="ResultatsRecherche/ResultatsRecherche_Alt1.screen#/"/>
  </panels>
</story:Storyboard>
